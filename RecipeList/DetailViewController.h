//
//  DetailViewController.h
//  RecipeTable
//
//  Created by Gary Davis on 3/1/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *recipeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *recipeImage;
@property (strong, nonatomic) IBOutlet UITextView *recipeIngr;
@property (strong, nonatomic) IBOutlet UITextView *recipeInst;


@property (strong, nonatomic) NSDictionary* info;

@end
