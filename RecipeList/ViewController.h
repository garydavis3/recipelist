//
//  ViewController.h
//  RecipeList
//
//  Created by Gary Davis on 3/2/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSArray* recipeList;
}


@end

