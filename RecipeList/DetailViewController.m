//
//  DetailViewController.m
//  RecipeTable
//
//  Created by Gary Davis on 3/1/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.recipeLabel.text = self.info[@"recipeName"];
    self.recipeIngr.text = self.info[@"recipeIngredient"];
    self.recipeInst.text = self.info[@"recipeInstruction"];
    
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: self.info[@"recipeImage"]]];
    self.recipeImage.image = [UIImage imageWithData: imageData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

